# Environments
| ROLE | DOMAIN | IPADDRESS |
|--- |--- |--- |
| **Master Node** | repo.localdomain | 192.168.56.113 |
| **nodeA** | servera.localdomain | 192.168.56.114 |
| **nodeB** | serverb.localdomain | 192.168.56.115 |
| **AAP** | https://serverb:8043/#/login | 192.168.56.115 |
| **PAH** | http://serverb:8000/ui/ | 192.168.56.115 |
| **Private Registry** | http://serverb:8000/v2/_catalog | 192.168.56.115 |
| **Gitlab** | http://gitlab-repo.localdomain:8080 | 192.168.56.113 |

# Commands
```
cp -R ../cert context/

# injection CA Zscaler
COPY cert/ /etc/pki/ca-trust/source/anchors
RUN update-ca-trust

```
# Machine Credentials
user: student  
pass: redhat  

# Gitlab Credentials
user: student  
pass: Student@123  

# AWX Credentials
user: admin  
pass: secret  

# PAH Credentials
user: admin  
pass: secret  

# Q01 - configure git
Do the basic configuration of Git by setting the following user name, email  
address, and default push method using the appropriate GIT commands  
User Name of GIT user should **student**  
The email ID of the GIT user should be **student@localdomain.com**  
The Method of Push to remote repository to be **simple**  
	
# Q02 - user create
Clone repo http://gitlab-repo.localdomain:8080/student/create_user.git in **esame** folder  
create a base **ansible.cfg** with privilege escalation enabled  
use as remote_user **student** and **redhat** as password  
Add the values called **name** of **potter** with **job** **developer** into **user_list.yml**  
Add the inventory **servera** and **serverb** to the group called **dev**  
Save, commit, and push your changes to the remote repository  
use ansible-navigator to run **exq02.yml**
	
# Q03 - Manage web server
Clone repo http://gitlab-repo.localdomain:8080/student/web_server.git in **esame** folder  
Manage the web server for the alias configuration, whenever the alias  
configuration is triggered the webserver should restart.  
Edit the **alias.yml** playbook in the git directory  
When the alias configuration is not triggered the webserver should not restart.
	
# Q04 - Manage web content
Clone repo http://gitlab-repo.localdomain:8080/student/web_content.git in **esame** folder  
Manage the web content as for the below instruction  
The task inside the task should executed based on the tags  
If the **content.yml** playbook is executed with tag **blue** only the first task is executed.  
If the **content.yml** playbook is executed with tag **green** only the second task is executed.  
If the tag is not mentioned, **none** of the task is executed.
	
# Q05 - Tune
Clone repo http://gitlab-repo.localdomain:8080/student/tunning_ansible.git in **esame** folder  
Ensure that the maximum number of simultaneous connection that the task inside the playbook can make is **15**  
Also Ensure that gathering facts is **disabled** for the future playbooks to be executed.
	
# Q06 - Create Multiple User
Clone repo http://gitlab-repo.localdomain:8080/student/create_complex_user.git in **esame** folder  
Create the users as per the dependent file specification.  
Create the playbook named the **complex_users.yml** and manage the **web_servers** nodes  
Users created on the managed node should have name as the **name** in the file **user_list.yml**  
The created user should have user **ID** as specified in the file **user_list.yml**  
The password for the user should be generated on the local directory on like **password-{name}** which the play book to be executed  
The generated password should have **6** unit **lengths** and it should implement the **SHA512** encryption method for the encryption  
The login name of the user should be in the order of **first**, **middle** and **last** name, separated with space  
and each name **1st** letter should be in **capital** case.
	
# Q07 - Use existing collection
Clone repo http://gitlab-repo.localdomain:8080/student/existing_collection.git in **esame** folder  
Install the **ansible.netcommon** collection in your machine from http://serverb:8000/ui/  
on the ~/esame/**collections**
	
# Q08 - Create a custom collection
Clone repo http://gitlab-repo.localdomain:8080/student/custom_collection.git in **esame** folder  
Create the custom collection named as **kerberos5.custom**  
Create the role user creation named as **newuser**  
Copy the **defaults.yml** content under **roles/newuser/defaults** directory location  
Copy the **meta.yml** content under **roles/newuser/meta** directory location  
Copy the **tasks.yml** content under **roles/newuser/tasks** directory location  
Copy the **vars.yml** content under **roles/newuser/vars** directory location  
Copy the **CHANGELOG.rst** in **kerberos5.custom** root directory collection  
check the created role launch **check-role.yml**  
Publish the created collection to private automation hub at http://serverb:8000/ui
	
# Q09 - Create a custom execution environment -1
Create execution environment named as **ee-user-environment:1.0**  
The image supports the Base image of **'serverb:8000/ee-minimal-rhel8:latest'**  
Use The following builder image **'serverb:8000/ansible-builder:latest'**  
it should contains the collection **kerberos5.custom** earlier configured  
check your job run with ansible-navigator **check-role.yml**  
Upload the image to **serverb:8000/ee-user-environment:1.0** repository.
	
# Q10 - Create a Custom execution environment-2
Create execution environment named as **ee-python-environment:1.0**  
The image supports the Base image of **'serverb:8000/ee-minimal-rhel8:latest'**  
Use The following builder image **'serverb:8000/ansible-builder:latest'**  
The environment requires:
* packages **python36** as dependent
* python package **python3-ldap** as dependent

Upload the image to **serverb:8000/ee-python-environment:1.0** repository.
		
# Q11 - Using custom EE
Clone repo http://gitlab-repo.localdomain:8080/student/dynamic_inventory.git in **esame** folder  
Note: **inventory.py** requires python packages **python36** and **python3-ldap**  
Create a script **main.sh** to run the playbook **main.yml** using **inventory.py**  
The playbook **main.yml** should be store the content **Sun set down in east** inside the **/etc/motd.d/banner** file  
should run on **web_servers** host only.
	
# Q12 - create play undefined vars
Clone repo http://gitlab-repo.localdomain:8080/student/master_playbook.git in **esame** folder  
Create a playbook named as **master_playbook.yml**  
use the **inventory.py** as host inventory  
Create a variable on the **master_playbook.yml**  
* **CONTENT**
* **FILE**
* **DIRECTORY**

Use the variable **CONTENT** to create content inside the **FILE** and the **FILE** should create under the **DIRECTORY**

# Q13 - using custom collections
Clone repo http://gitlab-repo.localdomain:8080/student/user_playbook.git in **esame** folder
create a playbook **cuser.yml** to create users  
Creating users by using the role newuser from **kerberos5.custom** collection  
use the collection **kerberos5.custom** in playbook and the playbook should run on **web_servers**
	
# Q14 - configure credentials in AWX
Open **AAP** (**AWX**) console: https://serverb:8043/#/login  
Create a source control credential (**SCC**)
* **Name**: EX374-GIT
* **Organization**: Default 
* **GitLab user**: student
* **password**: Student@123

Create a machine credential
* **Name**: EX374-MACHINE 
* **Organization**: Default
* Configure this credential for the **student** user
* **password**: redhat
* Use **sudo** as the privilege escalation method 
* Use **root** as the privilege escalation username
		
### PRIVATE REGISTRY (PAH)
Create credentials for private container registry 
* **Name**: EX374-private-registry
* **type**: Container Registry
* **Auth URL**: serverb:8000
* **user**: admin
* **pass**: secret

Create new custom **Execution Environment 1**
* **Name**: ee-user
* **Image**: serverb:8000/ee-user-environment:1.0
* **Pull**: Always pull container before running

Create new custom **Execution Environment 2**
* **Name**: ee-python
* **Image**: serverb:8000/ee-python-environment:1.0
* **Pull**: Always pull container before running
	
# Q15 - Project creation
Create **project 1**
* **Name**: EX374 copy file project
* **SCM TYPE**: Git
* **SCM URL**: http://gitlab-repo.localdomain:8080/student/master_playbook.git
* **SCC**: EX374-GIT

Create **project 2**
* **Name**: EX374 user project
* **SCM TYPE**: Git
* **SCM URL**: http://gitlab-repo.localdomain/student/user_playbook.git
* **SCC**: EX374-GIT
		
# Q16 - inventory
Create a **EX374 static** inventory:
* group **web_servers**
* hosts: **servera** and **serverb**

Create the **EX374 dynamic** inventory:
* using source from project **EX374 copy files project**  
* inventory source name as **EX374 Control source**  
* use as inventory **inventory.py**    
* using **ee-python** as execution environment
	
# Q17 - Template
Create **template 1**
* **NAME**: EX374 copy files
* **INVENTORY**: EX374 dynamic
* **PROJECT**: EX374 copy file project
* **PLAYBOOK**: master_playbook.yml

the job should uses the variables
```
---
CONTENT: "Moon rises after sun falls"
FILE: intro
DIRECTORY: /etc/motd.d
```

Create **template 2**
* **NAME**: EX374 user creation
* **INVENTORY**: EX374 static
* **PROJECT**: EX374 user project
* **PLAYBOOK**: cusers.yml
* **EE**: ee-user