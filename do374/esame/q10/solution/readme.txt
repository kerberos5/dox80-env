# build
podman build -f context/Containerfile -t quay.io/kerberos5/ee-python-environment:1.0 --layers=false context

# check
podman run -it --rm --name ee-python quay.io/kerberos5/ee-python-environment:1.0 /bin/bash
