# Ansible Project: ansible-ocp

Progetto ansible per la gestione e la configurazione base.\
di un cluster kubernetes basato su Openshift v.4x

## Requirements

* $ sudo yum install ansible -y
* $ sudo yum install python3-pip -y
* $ pip3 install openshift
* $ pip3 install jmespath (per utilizzare jsonQuery in ansible)
* $ ansible-galaxy collection install community.kubernetes
* $ ansible-galaxy collection install community.okd 
* $ ansible-galaxy collection list

## GIT Usage:
Prima di eseguire i playbook si raccomanda di aggiornare il progetto.

``git add --all``\
``git commit -m "aggiungi un commento"``\
``git push git@gitlab.com:kerberos5/ansible-ocp.git``\
``git pull git@gitlab.com:kerberos5/ansible-ocp.git``

## Running the playbook

### Example ansible Ad-Hoc
``ansible all -m ping -e@./vault_vars/vault_secret.yml``

### Example run playbook
``ansible-playbook -v playbook-example.yml``

### Master playbook
``` yaml
---
- name: "master"
  hosts: local
  gather_facts: false
  vars_files:
    - ./vault_vars/vault_secret.yml
  vars:
    ocp_user: admin
    ocp_pass: "{{ openshift_admin_password }}"
    kubeconfig_path: /home/devops/.kube/config
    ansible_python_interpreter: /usr/bin/python
  module_defaults:
    group/community.okd.okd:
      host: https://api.crc.testing:6443
      ca_cert: "{{ crc_path_ca }}/ca-cert.crt"

  tasks:
    - block:
      # accedo al cluster con utenza e genero il token
      - name: Log in (obtain access token)
        community.okd.openshift_auth:
          username: "{{ ocp_user }}"
          password: "{{ ocp_pass }}"
          validate_certs: true
        register: openshift_auth_results

      - name: my token
        debug:
          var: openshift_auth_results.openshift_auth.api_key

      ################# INSERISCI QUI LE ISTRUZIONI #################

      always:
      # efettuo il logout pulito
      - name: If login succeeded, try to log out (revoke access token)
        when: openshift_auth_results.openshift_auth.api_key is defined
        community.okd.openshift_auth:
          state: absent
          api_key: "{{ openshift_auth_results.openshift_auth.api_key }}"
```

## Note
note aggiuntive
