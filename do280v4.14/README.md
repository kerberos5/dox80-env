# LAB01 - Cluster Self-service Setup
* Create groups and users and associate users as follows

|GROUP|USER|
|---|---|
|platform|do280-platform|
|presenters|do280-presenter|
|workshop-support|do280-support|
|do280-attendees|do280-attendee|

* The **platform** group administers the cluster
* The **presenters** group consists of the people who deliver the workshops
* The **workshop-support** group maintains the needed applications to support the workshops and the workshop presenters
* Ensure that only users from the following groups can create projects:  

|GROUP|
|---|
|platform|
|presenters|
|workshop-support|

* An **attendee** must not be able to create projects. 
* Because this exercise requires steps that restart the Kubernetes API server, this configuration must **persist across API server restarts**
* The **workshop-support** group requires the following roles in the cluster:
  * The **admin** role to administer projects
  * A custom role that is provided in the **lab01/groups-role.yaml** file 
  * You must create this custom role to enable support members to create workshop groups and to add workshop attendees
* The **platform** group must be able to administer the cluster without restrictions
* The **workshop-support** group must perform the following tasks for the workshop project:
  * Create a workshop-specific attendees group
  * Assign the **edit** role to the attendees group
  * Add users to the attendees group
* All the resources that the cluster creates with a new workshop project must use **workshop** as the name for grading purposes
* Each workshop must enforce the following maximum constraints:
  * The project uses up to 2 CPUs
  * The project uses up to 1 Gi of RAM
  * The project requests up to 1.5 CPUs
  * The project requests up to 750 Mi of RAM
* Each workshop must enforce constraints to prevent an attendee's workload from consuming all the allocated resources for the workshop:
  * A workload uses up to 750m CPUs
  * A workload uses up to 750 Mi
* Each workshop must have a resource specification for workloads:
  * A default limit of 500m CPUs
  * A default limit of 500 Mi of RAM
  * A default request of 0.1 CPUs
  * A default request of 250 Mi of RAM
* Each workshop project must have this additional default configuration:
  * A local binding for the **presenter** user to the **admin** cluster role with the **workshop** name
  * The **workshop=project_name** label to help to identify the workshop workload
  * Must accept traffic only from within the same workshop by using the **workshop=project_name** label or from the ingress controller
* Use the **quay.io/redhattraining/hello-world-nginx:v1.0** image, to simulate a workshop workload
* As the **do280-presenter** user, you must create a workshop with the **do280** name
* As the **do280-support** user, you must create the **do280-attendees** group with the **do280-attendee** user
* Assign the **edit** role to the **do280-attendees** group to **do280** project


# LAB02 - Secure Applications
* Create the **workshop-support** namespace with the **category=support** label
* Workloads from the **workshop-support** namespace must enforce the following constraints:
  * The project uses up to 4 CPUs
  * The project uses up to 4 Gi of RAM
  * The project requests up to 3.5 CPUs
  * The project requests up to 3 Gi of RAM
* Define the default resource specification for workloads:
  * A default limit of 300m CPUs
  * A default limit of 400 Mi of RAM
  * A default request of 100m CPUs
  * A default request of 250 Mi of RAM
* Any quota or limit range must have the **workshop-support** name for grading purposes
* deploy the **project-cleaner** application from the **lab02/project-cleaner/example-pod.yaml** file to the **workshop-support** namespace 
* Using a **lab02/project-cleaner/cron-job.yaml** template to create a cron job that runs every minute
* The project cleaner deletes projects with the **workshop** label that exist for more than 10 seconds
* You must create a **project-cleaner-sa** service account to use in the project cleaner application
* The role that the project cleaner needs is defined in the **lab02/project-cleaner/cluster-role.yaml** file
* Deploy the **beeper-db** database in the **lab02/beeper-api/beeper-db.yaml** file to the **workshop-support** namespace
* Deploy the **beeper-api** application in the to the **workshop-support** namespace:
  * **lab02/beeper-api/beeper-api-deploy.yaml** file
  * **lab02/beeper-api/beeper-api-service.yaml** file
* You must configure this application to use TLS end-to-end by using the following specification:
  * Use the **beeper-api.pem** certificate and the **beeper-api.key** in the **lab02/certs** directory
  * Create secret with name **beeper-api-cert** and configure the **/etc/pki/beeper-api/** path as the mount point for the certificate and key
  * Set the **TLS_ENABLED** environment variable to the **true** value
* Update the **startup**, **readiness**, and **liveness** probes to use **TLS**
* Create a passthrough route with the **beeper-api.apps.ocp4.redhat.ren** hostname
* You can verify with URL
  *  https://beeper-api.apps.ocp4.redhat.ren/api/beeps
  *  https://beeper-api.apps.ocp4.redhat.ren/swagger-ui/index.html
* Verify that you can access the **beeper-db** service from the **workshop-support** namespace by testing TCP connectivity to the database service. Use the oc debug command to create a pod with the nc command with the -z option to test TCP access  

``` bash
$ oc debug --to-namespace="workshop-support" -- nc -z -v beeper-db.workshop-support.svc.cluster.local 5432
```
* Create an entry in the database with the **swagger-ui** https://beeper-api.apps.ocp4.redhat.ren/swagger-ui/index.html#/beep/create or can use the following curl command:

``` bash
$ curl -s --cacert /home/mobaxterm/cert/ca-beeper.crt -X 'POST' \
  'https://beeper-api.apps.ocp4.redhat.ren/api/beep' \
  -H 'Content-Type: application/json' \
  -d '{ "username": "user1",  "content": "first message" }'
```

* Create a Network Policy with name **database-policy** as follows:
  * Apply the policy to database pods, which are pods in the **workshop-support** namespace with the **app=beeper-db** label
  * They must accept only TCP traffic from the **beeper-api** pods in the **workshop-support** namespace on the **5432** port
  * You can use the **category=support** label to identify the pods that belong to the **workshop-support** namespace
* Create a Network Policy with name **beeper-api-ingresspolicy** as follows:
  * Configure the cluster network so that the **workshop-support** namespace accepts only external ingress traffic to pods that listen on the **8080** port, and blocks traffic from other projects

# LAB03 - Deploy Packaged Applications
Deploy an application that uses a database by using a Helm chart and Kustomization files. Access the application by using a route
* Use a **compreview-package** project for all the resources
* Deploy Nginx webserver named **my-nginx** with **0.1.2** chart version using Helm chart in the http://tmvlocp.sidi.mpi.it:8000/charts/ repo
* Use Kustomize in the path **lab03/quotes** to deploy the application. Add a new Kustomize **production** overlay that adds **2** replicas to the application and production route: https://prod-quotes.apps-crc.testing