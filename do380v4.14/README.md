# EX380v4.14
## Task01
Configure custom IPD with the following requirments:  

|KEY|VALUE|
|--- |--- |
|IDP Name|LDAP-AUTH-EX380|
|Protocol|ldaps|
|LDAP Server|ldap-dev.sidi.mpi.it:10636|
|Bind DN|cn=admin,dc=redhat,dc=ren|
|Bind Password|secret|
|Insecure|false|
|Base DN|ou=users,dc=redhat,dc=ren|
|Query filter|uid|
|Mapping Method|claim|

* The project **openshift-config** contains a generic secret named **ldap-auth-secret** with bind password
* The project **openshift-config** contains a configuration map named **ldap-auth-ca** with content from  
  http://tmvlocp.sidi.mpi.it:8000/ipa/config/ca.crt and preserves the original name of the certificate
* The attributes **id=dn**, **email=mail**, **name=cn** and **preferredUsername=uid** are configured
* The user **Daniele** must be able to log in using the **review**

## Task02
Configure your openshift cluster for this audit with the following requirments:  

* A client certificate exists with the username **acme-auditor**
* A group exits with the name **acme-corp**
* Members of this group have access to the cluster role **cluster-reader**
* The client certificate exists in **~/access/kubeconfig-acme.config**
* The client certificate must not be able to create or delete projects
* The client certificate must be able to view all pods in the cluster

## Task03
Restore the application as per the following requirements:
* The latest completed scheduled backup is used to create a restore caleed **wiki-restore**
* The application generate output
* All failed backup are deleted

## Task04
Deploy the openshift gitops operator according to the following requirements:
* The operator is installed in the openshift-gitops-operator project
* The ArgoCD instance deployed by the operator has TLS enabled with reencrypt termination
* The **admin** user is member of the **gitops-admin** group
* The **gitops-admin** group is the only configured as **role:admin** with RBAC key policy
* An instance GIT repository exits at http://gitlab-dev.sidi.mpi.it:8080/devops/ocp-gitops.git with Skip server verification enabled
* An application named **machineconfig-motd-deploy** exits in the ArgoCD with the Sync Policy configured as **Manual**
*  The GIT repository contains two MachineConfig resources as follows:
   * A file called **71-master-sshd-motd.yaml** which uses the latest ignition version with
      * A path exists called **/etc/motd**
      * The filesystem is **root**
      * The overwrite policy is **true**
      * This object is applied only on the nodes marked with: machinconfiguration.openshift.io/role: **master**
   * A file called **71-worker-sshd-motd.yaml** which uses the latest ignition version with the
      * A path exits called **/etc/motd**
      * The filesystem is **root**
      * The overwrite policy is **true**
      * This object is applied only on the nodes marked with machineconfiguration.openshift.io/role: **worker**
* In both cases, the machine configuration create a file located at http://tmvlocp.sidi.mpi.it:8000/motd/ssh.motd
* set the permission on /etc/motd on all nodes r--r--r--

## Task05
Configure the **openshift-logging** project in your openshift cluster to forward logs to an external aggregator according to the following specifications:
* The log collector type is **vector**
* Application and infrastructure logs are forwarded to the syslog service on host **log.sidi.mpi.it**
* Audit logs are forwarded to the syslog service on host **log.sidi.mpi.it**
* The syslog service on both hosts listens on port **tcp/514** and port **udp/514**
* Application logs are tagged with **procID: app**
* Infrastructure logs are tagged with **procID: infra**
* Audit logs are tagged with **procID: audit**
* All logs are tagged with **appName: openshift**
* The Event Route us running using registry image: registry.redhat.io/openshift-logging/eventrouter-rhel8:v0.4
* The syslog server have already been fully configured for you to receive logging messages and filter them using the procID attribute to the following files:
  * /var/log/ex380-app.log
  * /var/log/ex380-infra.log
  * /var/log/ex380-audit.log

## Task06
A work colleague accidentally misconfigured your openshift cluster prior to leaving on vacation.  
As a result, application pods are not able to run.  
Diagnose and correct the problem so that application pods are able to run on any worker node  
You may use the image from **quay.io/redhattraining/hello-openshift** to verify your work.  
The image must deploy into a Running container without intervention.