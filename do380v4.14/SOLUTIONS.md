## Task01
``` bash
# estrai una copia di backup di oauth/cluster
$ oc get oauth/cluster -o yaml > oauth.yaml

# Crea secret con bindPassword
$ oc create secret generic ldap-auth-secret --from-literal bindPassword=secret -n openshift-config

# Scarica la CA del server ldap e crea la configmap
$ wget http://tmvlocp.sidi.mpi.it:8000/ipa/config/ca.crt
$ oc create configmap ldap-auth-ca --from-file=ca.crt -n openshift-config

# crea da console IPD custom con nome LDAP-AUTH-EX380
```

Esempio di come dovrebbe apparire il manifest

``` yaml
spec:
  identityProviders:
  - htpasswd:
      fileData:
        name: htpass-secret
    mappingMethod: claim
    name: developer
    type: HTPasswd
  - ldap:
      attributes:
        email:
        - mail
        id:
        - dn
        name:
        - cn
        preferredUsername:
        - uid
      bindDN: cn=admin,dc=redhat,dc=ren
      bindPassword:
        name: ldap-auth-secret
      ca:
        name: ldap-auth-ca
      insecure: false
      url: ldaps://ldap-dev.sidi.mpi.it:10636/ou=users,dc=redhat,dc=ren?uid
    mappingMethod: claim
    name: LDAP-AUTH-EX380
    type: LDAP
  templates:
    login:
      name: login-template
  tokenConfig:
    accessTokenMaxAgeSeconds: 0
```
``` bash
# Verifica il corretto caricamento dei pod di authentication e testa l'utente
$ watch oc get pod -n openshift-authentication
$ oc login -u Daniele -p ******
```

## Task02
``` bash
# creo utente e gurppo, fornisco al gruppo i permessi di cluster-reader
$ oc adm groups new acme-corp
$ oc adm groups add-users acme-corp acme-auditor
$ oc adm policy add-cluster-role-to-group cluster-reader acme-corp

# crea directory di appoggio
$ mkdir access

# Creo csr per l'untenza acme-auditor
$ openssl req -newkey rsa:4096 -nodes -keyout server.key -subj "/O=acme-corp/CN=acme-auditor" -out acme-corp.csr

# Codifica in base64 la csr
$ cat acme-corp.csr | base64 -w0 > encode

# Crea un manifest per la request
$ oc explain CertificateSigningRequest --recursive
$ oc explain CertificateSigningRequest.spec
```
Ecco un esempio di come deve essere fatta  

``` yaml
apiVersion: certificates.k8s.io/v1
kind: CertificateSigningRequest
metadata:
  name: acme-auditor-access
spec:
  signerName: kubernetes.io/kube-apiserver-client
  expirationSeconds: 604800
  request: |
    # inserisci la csr codificata in base64
  usages:
    - client auth
```
``` bash
# applica il template e approva la csr
$ oc apply -f request.yaml
$ oc get csr
$ oc adm certificate approve acme-auditor-access

# estrai il certificato client dalla csr
$ oc get csr acme-auditor-access -o jsonpath='{.status.certificate}' | base64 -d > aceme-auditor-access.crt

# estrai la CA dal server ocp
$ openssl s_client -connect api.crc.testing:6443 -showcerts
# prendi il primo certificato visibile e copialo in ocp-apiserver-cert.crt
# oppure (un po più complicato)
$ openssl s_client -connect api.crc.testing:6443 -showcerts </dev/null 2>/dev/null|openssl x509  -outform PEM > ocp-apiserver-cert.crt

# crea kubeconfig custom
$ oc config set-cluster api-crc-testing \
--certificate-authority=ocp-apiserver-cert.crt \
--server=https://api.crc.testing:6443 \
--kubeconfig=kubeconfig-acme.config

$ oc config set-credentials acme-auditor \
--client-certificate=acme-auditor-access.crt \
--client-key=server.key \
--embed-certs \
--kubeconfig=kubeconfig-acme.config

$ oc config set-context acme-auditor \
--cluster=api-crc-testing \
--namespace=default \
--user=acme-auditor \
--kubeconfig=kubeconfig-acme.config

$ oc config use-context acme-auditor --kubeconfig=kubeconfig-acme.config

# Prova l'accesso con l'unteza acme-auditor
$ oc whoami --kubeconfig kubeconfig-acme.config
$ oc get pod -A --kubeconfig kubeconfig-acme.config
```

## Task03
``` bash
# mappa il binario velero dal container
$ alias velero='oc -n openshift-adp exec -it deployment/velero -c velero bash -- ./velero'

# Verifica la presenza dei backup
$ velero get backup

# Crea restore da backup con nome wiki-restore
velero restore create wiki-restore --from-backup <backup_name>

# ricava tutti i backup falliti
$ velero get backup | grep Failed

# rimuovi backup falliti in unica soluzione
for backup in $(velero get backup | grep Failed | awk '{print $1}'); do
  velero delete backup $backup --confirm
done

# verifica app
curl -u admin:Workload1854 -k https://lynx.apps.domain12.example.com
```

## Task04
Installa l'operatore **Red Hat OpenShift GitOps** da console lasciando tutto di default  
Una volta terminata l'installazione edita l'istanza di default nel tab ArgoCD  
Rimuovi completamente il blocco **ownerReferences** dal manifest YAML (altrimenti l'operatore ripristinerà le modifiche fatte)
Cambia la tipologia della route come segue:

``` yaml
# da rimuovere completamente
  ownerReferences:
    - apiVersion: pipelines.openshift.io/v1alpha1
      blockOwnerDeletion: true
      controller: true
      kind: GitopsService
      name: cluster
      uid: eacb3ca4-b70c-4e84-8cb4-cedf41b81ac9

# modifica la route
    route:
      enabled: true
      tls:
        termination: reencrypt
    service:
      type: ''
```
Accedi alla console di ArgoCD  

``` bash
# crea gruppo gitops-admin e aggiungi l'utente admin al gruppo
$ oc get groups
$ oc adm groups new gitops-admin
$ oc adm policy add-cluster-role-to-group cluster-admin gitops-admin
$ oc adm groups add-users gitops-admin admin
```

Edita nuovamente l'istanza ArgoCD e aggiungi alla sezione RBAC il gruppo creato sopra:

``` yaml
  rbac:
    defaultPolicy: ''
    policy: |
      g, system:cluster-admins, role:admin
      g, cluster-admins, role:admin
      g, gitops-admin, role:admin
    scopes: '[groups]'
```

Esegui l'injection della configmap

``` bash
$ oc project openshift-gitops
$ oc create configmap cluster-root-ca-bundle
$ oc label configmap/cluster-root-ca-bundle config.openshift.io/inject-trusted-cabundle=true
```

Monta la configmap appena creata nell'istanza di ArgoCD
``` bash
# cerca la posizione nel container
$ oc rsh pod/openshift-gitops-repo-server-64dc4fd6f-dkvt9 bash -c 'ls -l /etc/pki/ca-trust/extracted/pem/tls-ca-bundle.pem'

# Cerca la chiave della configmap
$ oc get cm cluster-root-ca-bundle -o json | more
```
Edita l'istanza di ArgoCD da console ricerca "repo" e montala come segue subito sotto alla sezione resources

``` yaml
    volumeMounts:
      - mountPath: /etc/pki/ca-trust/extracted/pem/tls-ca-bundle.pem
        name: cluster-root-ca-bundle
        subPath: ca-bundle.crt
    volumes:
      - configMap:
          name: cluster-root-ca-bundle
        name: cluster-root-ca-bundle
```
Verifica che sia stata correttamente montata
``` bash
$ oc get pod
$ oc rsh pod/openshift-gitops-repo-server-86cd464b86-cd92k bash -c 'ls -l /etc/pki/ca-trust/extracted/pem/'
```

Clone repo
``` bash
$ git clone http://gitlab-dev.sidi.mpi.it:8080/devops/ocp-gitops.git
$ mkdir -p ocp-gitops/apps/machineconfig
$ cd ocp-gitops/apps/machineconfig

# Crea il file di benvenuto http://tmvlocp.sidi.mpi.it:8000/motd/ssh.motd
$ echo "Welcome to ocp cluster!" > /var/www/html/motd/ssh.motd

# Genera l'hash del file
$ sudo sha256sum /var/www/html/motd/ssh.motd | awk '{print $1}'
```
Crea un manifest per il nodo worker come segue.  
Ti puoi aiutare ad esempio con una machineConfig esistente  

``` bash
$ oc get mc 99-worker-chrony-override... -o yaml
```
Ricostruisci il manifest partendo dall'esempio sopra e commita su git

``` yaml
apiVersion: machineconfiguration.openshift.io/v1
kind: MachineConfig
metadata:
  labels:
    machineconfiguration.openshift.io/role: worker
  name: 71-worker-sshd-motd
spec:
  baseOSExtensionsContainerImage: ""
  config:
    ignition:
      version: 3.2.0
    storage:
      files:
      - contents:
          source: http://tmvlocp.sidi.mpi.it:8000/motd/ssh.motd
          verification:
            hash: sha256-a42a819f4b7354f15caaa4c147e136560b3cc8b92fef28bb9c1fdbcf4fd94f63
        mode: 0444
        overwrite: true
        path: /etc/motd
``` 

``` bash
$ git add --all
$ git commit -m 'add 71-worker-sshd-motd'
$ git push

# ripeti la stessa procedura per i nodi master creando un manifest con nome 71-master-sshd-motd.yaml
```

Crea pipeline su ArgoCD accedi alla console e crea la pipeline così

|KEY|VALUE|
|--- |--- |
|Application Name|machineconfig-motd-deploy|
|Project Name|default|
|Sync|Manually|
|Retry|Checked|
|Repository URL|http://gitlab-dev.sidi.mpi.it:8080/devops/ocp-gitops.git|
|Path|apps/machineconfig|
|Cluster URL|https://kubernetes.default.svc|

> ⚠️ **Attenzione!**  
> La pipeline è creata con **sincronizzazione Manuale**  
> Per avviarla, lanciala manualmente,  
> ma è **altamente sconsigliato**  
> _(come testato localmente, la pipeline manda i nodi in degraded!)_

## Task05
Installa operatore **OpenShift Logging v.5.8.x** da Operator HUB  
Crea la CR ClusterLogging come segue:
``` yaml
apiVersion: logging.openshift.io/v1
kind: ClusterLogging
metadata:
  name: instance
  namespace: openshift-logging
spec:
  managementState: Managed
  collection:
    type: vector
```

Crea la CR ClusterLogForwarder come nell'esempio seguente:
``` yaml
apiVersion: logging.openshift.io/v1
kind: ClusterLogForwarder
metadata:
  name: instance
  namespace: openshift-logging
spec: 
  outputs: 
    - name: audit-syslog 
      type: syslog
      syslog:
        appName: openshift
        procID: audit
        rfc: RFC5424
      url: 'tcp://log.sidi.mpi.it:10514'

    - name: audit-syslog-udp
      type: syslog
      syslog:
        appName: openshift
        procID: audit 
        rfc: RFC5424
      url: 'udp://log.sidi.mpi.it:10514'

    - name: app-syslog
      type: syslog
      syslog:
        appName: openshift
        procID: app
        rfc: RFC5424
      url: 'tcp://log.sidi.mpi.it:10514'

    - name: app-syslog-udp
      type: syslog 
      syslog: 
        appName: openshift 
        procID: app 
        rfc: RFC5424
      url: 'udp://log.sidi.mpi.it:10514'

    - name: infra-syslog
      type: syslog
      syslog:
        appName: openshift
        procID: infra
        rfc: RFC5424
      url: 'tcp://log.sidi.mpi.it:10514'

    - name: infra-syslog-udp
      type: syslog
      syslog:
        appName: openshift
        procID: infra
        rfc: RFC5424
      url: 'udp://log.sidi.mpi.it:514'

  pipelines:
    - name: audit-to-syslog
      inputRefs: 
        - audit 
      outputRefs: 
        - audit-syslog 
        - audit-syslog-udp

    - name: app-to-syslog 
      inputRefs: 
        - application
      outputRefs: 
        - app-syslog 
        - app-syslog-udp

    - name: infra-to-syslog 
      inputRefs: 
        - infrastructure
      outputRefs: 
        - infra-syslog 
        - infra-syslog-udp
```
``` bash
# verifica che sia tutto app e running
$ oc get daemonset -n openshift-logging
```

Installa event router da template **eventrouter.yml**
``` bash
$ oc process -f eventrouter.yml | oc apply -f -
```

Verifica che vengano collezionati i logs
``` bash
$ ls -l /srv/syslog/log
```

Per creare e installare il template per eventrouter
``` bash
$ oc create -f dox80-env/do380v4.14/res/eventrouter-template.yaml -n openshift-logging
$ oc process -n openshift-logging eventrouter-template --parameters

# se voglio specificare un paramentro
$ oc process -n openshift-logging eventrouter-template \
-p IMAGE="registry.domain12.example.com/openshift-logging/eventrouter-rhel9:8.8" | oc apply -f -

# per processare le risorse del template
$ oc process eventrouter-template -n openshift-logging | oc apply -f -
$ oc get all -l component=eventrouter

# per rimuovere tutte le risorse create 
$ oc process eventrouter-template -n openshift-logging | oc delete -f -
```

## Task06
``` bash
$ oc new-project test
$ oc new-app --name hello --image quay.io/redhattraining/hello-openshift

# Verifica eventuali taint sui nodi
$ oc get nodes
$ oc describe node | grep -i taint 

# RImuovi eventuali taint dai nodi worker
oc adm taint node worker01 ex380:NoSchedule-
```