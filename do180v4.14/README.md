# Comprehensive Review
## Deploy Web Applications
* The API URL of your OpenShift cluster is https://api.crc.testing:6443.  
* The URL of the OpenShift web console is https://console-openshift-console.apps-crc.testing.
* The password for the **admin** user is **review**    
* Create a project named **review** to store your work
* Configure your project so that its workloads refer to the database **image** by the **mysql8:1** short name
* The short name must point to the **registry.redhat.io/rhel9/mysql-80:1-228** contaier image. The database image name and its source registry are expected to change in the near future, and you want to isolate your workloads from that change.
* Ensure that the workload resources in the **review** project can use the **mysql8:1** resource  
* Create the **dbparams** secret to store the MySQL database parameters. Both the **database** and the **front-end** deployment need these parameters. The dbparams secret must include the following variables:  

| KEY | VALUE |
|--- |--- |
|user|operator1|
|password|redhat123|
|database|quotesdb|

Create the **quotesdb** deployment and configure it as follows
* Use the **mysql8:1** image for the deployment
* The database must automatically roll out whenever the source container in the **mysql8:1** resource changes
* To test your configuration, you can change the **mysql8:1** image to point to the **registry.redhat.io/rhel9/mysql-80:1-237** and then verify that the **quotesdb** deployment rolls out. 
Remember to reset the **mysql8:1** image to the **registry.redhat.io/rhel9/mysql-80:1-228**
* Define the following environment variables in the deployment from the keys in the dbparams secret:  

| KEY | VALUE |
|--- |--- |
|MYSQL_USER|user|
|MYSQL_PASSWORD |password|
|MYSQL_DATABASE|database|

* Ensure that OpenShift preserves the database data between pod restarts. This data does not consume more than **2 GiB** of disk space. The MySQL database stores its data under the **/var/lib/mysql** directory. Use the **dynamic** storage class for the volume
*  Create a **quotesdb** service to make the database available to the front-end web application. The database service is listening on port **3306**  
Create the **frontend** deployment and configure it as follows:
* Use image **quay.io/redhattraining/famous-quotes:2.1**. For this deployment, you refer to the image by its full name
* Define the following environment **variables** in the deployment:  

| KEY | VALUE |
|--- |--- |
|QUOTES_USER|The user key from the dbparams secret|
|QUOTES_PASSWORD|The password key from the dbparams secret|
|QUOTES_DATABASE|The database key from the dbparams secret|
|QUOTES_HOSTNAME|quotesdb|

* Expose the frontend deployment so that the application can be reached at https://frontend.apps-crc.testing
* The **frontend** deployment is listening to port **8000**

## Troubleshoot and Scale Applications
* The API URL of your OpenShift cluster is https://api.crc.testing:6443.  
* The URL of the OpenShift web console is https://console-openshift-console.apps-crc.testing.
* The password for the **admin** user is **review**
* Create a project named **compreview-scale**  
* Create the **quotesdb** from **registry.redhat.io/rhel9/mysql-80:1-228** contaier image.
* Create the **frontend** from **quay.io/redhattraining/famous-quotes:2.1** contaier image.
Fix the application and make it ready for production:  
* The **quotesdb** deployment in the **compreview-scale** project starts a MySQL server, but
the database is failing. Review the logs of the pod to identify and then fix the issue    
Use the following parameters for the **database**:

| KEY | VALUE |
|--- |--- |
|Username|operator1|
|Password|redhat123|
|Database name|quotes|

Use the following parameters for the **frontend**:

| KEY | VALUE |
|--- |--- |
|QUOTES_USER|operator1|
|QUOTES_PASSWORD|redhat123|
|QUOTES_DATABASE|quotes|
|QUOTES_HOSTNAME|quotesdb|  

* You security team validated a new version of the MySQL container image that fixes a security issue.
* The new container image is **registry.redhat.io/rhel9/mysql-80:1-237**  
* Update the **quotesdb** deployment to use this image. Ensure that the database redeploys  
* Add a **probe** to the **quotesdb** deployment so that OpenShift can detect when the database is **ready** to accept requests
* Use the **mysqladmin** **ping** command for the probe
* Add a second **probe** that **regularly** verifies the status of the database. Use the **mysqladmin** **ping** command as well
* Configure CPU and memory usage for the **quotesdb** deployment.
* The deployment needs **200** **millicores** of CPU and **256** **MiB** of memory to run
* You must restrict its CPU usage to **500** **millicores** and its memory usage to **1** **GiB**
* Add a probe to the **frontend** deployment so that OpenShift can detect when the web application is **ready**
* The application is ready when an HTTP request on port **8000** to the **/status** path is successful
* Add a second **probe** that **regularly** verifies the status of the web front end.
* The front end works as expected when an HTTP request on port **8000** to the **/env** path is successful
* Configure **CPU** and **memory** usage for the frontend deployment.
* The deployment needs **200** **millicores** of CPU and **256** **MiB** of memory to run.
* You must restrict its CPU usage to **500** **millicores** and its memory usage to **512** MiB
* Scale the **frontend** application to **three** pods to accommodate for the estimated production load
* To verify your work, access the https://frontend-compreview-scale.apps-crc.testing
