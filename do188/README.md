# Welcome to the EX188 Exam Simulation

## requirements:

* Account GitLab
* Account quay.io
* Account registry.redhat.io
* Account docker.io

# TASK01:

Install podman on the workstation machine and add **repo.localdomain:5000**  
as an insecure registry. Also make sure that podman searches this registry before trying any others.

# TASK02:

Start a new container with the following parameters:  
* Name: **basics-podman-server**
* Image: registry.access.redhat.com/ubi8/httpd-24:latest
* Ports: Route traffic from port **8080** on your machine to port **8080** inside of the container
* Network: lab-net  

Copy the ~/dox80-env/do188/index.html file to /var/www/html/ in  
the **basics-podman-server** container. Start a new container with the following parameters:  

* Name: **basics-podman-client**
* Image: registry.access.redhat.com/ubi8/httpd-24:latest
* Network: lab-net

Confirm that the **basics-podman-client** container can access the  
**basics-podman-server** container by its DNS name.

Use the podman exec and curl commands to make a request to the  
**basics-podman-server** container at port **8080** from the  
**basics-podman-client** container.

# TASK03:

* Build a container image that uses the ~/dox80-env/do188/python-server/Containerfile file.
* Call the resulting image **python-server** and push the image into docker.io/kerberos5/python-server public repo
* Add the rootless tag to the **images-lab** container image, and push it to the docker.io/kerberos5/python-server  
* Run now this command:
```
sudo cp /etc/subgid /etc/subgid_bck
sudo cp /etc/subuid /etc/subuid_bck 
sudo > /etc/subuid
sudo > /etc/ subgid
podman system migrate
```
* Create a container name **python-server** use image **python-server:rootless**  
and bind the **8080** container port to the **8080** host port
* Start the container with **-d –rm** flags 
* Check why the container doesn't start
* You can use this parameters for fix the problem:
```
subuids: 100000-165535
subgids: 100000-165535
```
* Use the curl command to verify that the HTTP server is running

# TASK04:

* Create container using custom image multi-stage:
* Complete the ~/dox80-env/do188/nginx-certs/Containerfile with multi-stage strategy
* follow the instructions inside the Containerfile
* Expose on port **8080:8080 **and **8443:8443** container named nginx-mstage
* Optionally, use the curl command to verify that the HTTP server is running

# TASK05:

* Create a directory for ~/DO188/labs/persisting-databases
* Copy the scripts from ~/dox80-env/do188/persisting-databases/* to ~/DO188/labs/persisting-databases
* Create a local volume named **rpi-store-data**
* Create a private network named **persisting-network**
* Create a container in detach mode (-d) from image:
* registry.redhat.io/rhel8/postgresql-12:latest with follows parameters:

| KEY | VALUE |
|--- |--- |
|Name:|persisting-pg12|
|POSTGRESQL_USER:|backend|
|POSTGRESQL_PASSWORD:|secret_pass|
|POSTGRESQL_DATABASE:|rpi-store|
|Startup volume:|~/DO188/labs/persisting-databases:/opt/app-root/src/postgresql-start|
|Volume data persistent:|rpi-store-data:/var/lib/pgsql/data|
|network:|persisting-network|

* Verify data into db **rpi-store** and types table are present run:
```
"psql -U backend -d rpi-store -c 'select \* from types'"
```
* Create a new container in detach mode from image: docker.io/dpage/pgadmin4:latest with follows parameters:

| KEY | VALUE |
|--- |--- |
|Name:|persisting-pgadmin|
|Port:|5050:80|
|PGADMIN_DEFAULT_EMAIL=|trb.dnl@gmail.com|
|PGADMIN_DEFAULT_PASSWORD=|secret|
|network:|persisting-network|

* Log in [<u>http://servera:5050</u>](http://servera:5050/) and add new db **rpi-store** in connection tab:

| KEY | VALUE |
|--- |--- |
|Host:|persisting-pg12|
|User:|backend|
|Pass:|secret_pass|

* Verify thah data are present into **types** table
* Run the ~/dox80-env/do188/persisting-databases/**pg_dump.sh** script to create dump
* Stop the **persisting-pg12** container
* Create a volume called **rpi-store-data-pg13** to store the database files
* Create a **persisting-pg13** container
* use **rpi-store-data-pg13** volume
* use same parameters of **persisting-pg12**
* image registry.redhat.io/rhel9/postgresql-13:1
* Run the ~/dox80-env/do188/persisting-databases/pg_restore.sh script to import dump

# TASK06:

* Connect to **RHOCP** with follows credentials:

| KEY | VALUE |
|--- |--- |
|User:|admin|
|Pass:|review|
|API:|https://api.crc.testing:6443|

* Log in into registry default-route-openshift-image-registry.apps-crc.testing using podman  
* In to default project import image registry.access.redhat.com/ubi8/**ubi-minimal:8.5**
* check the presence of the image from podman into **RHOCP** internal repository
* With **skopeo** copy the image from the internal repository to the public repo quay.io/kerberos5/ubi-minimal:8.5

# TASK07:

* Observe the file ~/dox80-env/do188/podman-arg/Containerfile
* Build image named **podman-arg:do188** e run container in **--rm** mode
* Verify that the container prints **DO188** once started
* Now build a new image named named **podman-arg:do380** so that the container prints **DO380** 
* Do not modify the Containerfile!

# TASK08:

* Create container **MariaDB**
* Run the container in the background, with the following parameters:
* You will again use the image **mariadb** from the registry on repo.localdomain:5000 
* Give the container the name **secretsdb**
* Publish port **3306** on external port **3307**
* The variables for database startup should be set as secret environment variables.

| KEY | VALUE |
|--- |--- |
|MYSQL_USER=|duffman|
|MYSQL_PASSWORD=|saysoyeah|
|MYSQL_ROOT_PASSWORD=|SQLp4ss|
|MYSQL_DATABASE=|beer|

* Attach the **~/mariadb** directory as a volume that maps to **/var/lib/mysql**
* Stop container and refactor your configuration in such a way that you create four Podman secrets: 
* mysql_user: **duffman**, from file **~/mysql_user**
* mysql_password: **saysoyeah**, from file **~/mysql_password**
* mysql_root_password: **SQLp4ss**, from file **~/mysql_root_password**
* mysql_database: **beer**, from file **~/mysql_database**
* Once the container is up and running, prove that your settings are correctly applied. 
* Connect to your database as the root user and check for the existence of the beer database:

```
echo "show databases;" \| mysql -uduffman -h 127.0.0.1 -P3307 -psaysoyeah
```

* Run the command located in ~/dox80-env/do188/sql/beer.sql to insert values into the database. 
```
mysql -uduffman -h 127.0.0.1 -P3307 -psaysoyeah \< ~/dox80-env/do188/sql/beer.sql
```

* Verify with query: 

```
echo "select \* from beer.types;" \| mysql -uduffman -h 127.0.0.1 -P3307 -psaysoyeah
```

# TASK09:

* Create a Dockerfile which creates a container image with the following requirements. 
* Store the Dockerfile as ~/dox80-env/do188/task09/task09.dockerfile
* During the build, create a user account. **joe** must be the default user.
* Read an argument during build-time to override the name **joe** The container must run **whoami** to show the active user.

```
NOTE: Arguments are NOT passed during runtime of the container! They are passed during the container build.
```

* Create two container images using this Dockerfile, named **hello-joe:1.0** and **hello-lisa:1.0**
* When run, they should respectively output **joe** and **lisa**

# TASK10:

* This task heavily leans on the Docker Samples project's Voting App.
* Use the incomplete Podman Stack file located in > ~/dox80-env/do188/voting/docker-compose.yml
* Use the comments in the file to guide you through the process of creating a Podman Stack with the following requirements
* There are two **networks**: 

| KEY | VALUE |
|--- |--- |
|front-end| (of type bridge)| 
|back-end| (of type internal)| 

* There is one **volume**:

| KEY | VALUE |
|--- |--- |
|db-data| (of type local)|  

The Stack consists of five services:
* Create service named **redis** using the docker.io/redis:latest image, in network **back-end**
* Create service named **db** using the docker.io/postgres:9.4.26-alpine image, in network **back-end**  
  with volume **db-data** mounted on /var/lib/postgresql/data/
* the Postgres container needs to have two environment variables: **POSTGRES_USER** and **POSTGRES_PASSWORD**, both set to **postgres**
* Create service named **vote** using the docker.io/dockersamples/examplevotingapp_vote image, in networks **back-end** and in **front-end**
* the **vote** container exposes container port **80** on public port **5000**. It depends on **redis**
* Create service named **result** using the docker.io/dockersamples/examplevotingapp_result image, in networks **back-end** and in **front-end**
* the **result** container exposes container port **80** on public port **5001**. It depends on **db**
* Create service named **worker** using the docker.io/dockersamples/examplevotingapp_worker image, in networks **back-end**
* the worker container depends on both **db** and **redis**

# TASK11:

* Use the incomplete Podman Stack file located in ~/dox80-env/do188/pgadmin/docker-compose.yml 
* Use the comments in the file to guide you through the process of creating a Podman Stack with the following requirements
* Create service named **persisting-pg12** based on image: registry.redhat.io/rhel8/postgresql-12:latest 
* Create service named **persisting-pgadmin** based on image: docker.io/dpage/pgadmin4:latest this service depend by **persisting-pg12**
* Log in after startup to [**http://servera:5050**](http://servera:5050/) and enter the

| KEY | VALUE |
|--- |--- |
|User:|trb.dnl@gmail.com|
|Pass:|secret|

* Check that the connection to the database **rpi-store** is successful and that the data is present in the types table

# TASK12:

* Deploy **postgresql-12** and pgadmin in RHOCP uses the parameters in the previous exercise (**TASK05**)
* Create app from image: registry.redhat.io/rhel8/postgresql-12:latest with follows parameters:

| KEY | VALUE |
|--- |--- |
|Name:| persisting-pg12|
|POSTGRESQL_USER=|backend|
|POSTGRESQL_PASSWORD=|secret_pass|
|POSTGRESQL_DATABASE=|rpi-store|

* Create a **PV** named **rpi-store-pv** storage **1Gi** storageclass **nfs-storage** access mode **RWO**
* Create a **PVC** named **rpi-store-pvc** for namespace test storage **1Gi** storageclass **nfs-storage** access mode **RWO**
* Mount PVC create in /var/lib/pgsql/data
* Perform data phasing using the script ~/dox80-env/do188/persisting-databases/rpi-store-ddl.sql
* Run the query "**select * from types**" to verify correct data entry
* Create a new app from image: docker.io/dpage/pgadmin4:latest with follows parameters: 

| KEY | VALUE |
|--- |--- | 
|Name:|persisting-pgadmin|
|PGADMIN_DEFAULT_EMAIL=|trb.dnl@gmail.com|
|PGADMIN_DEFAULT_PASSWORD=|secret|

* Create serviceaccount named **pgadmin** add **SCC** permissions to the service account and fix the deployment
* Create a edge route for **pgadmin** service with hostname: persisting-pgadmin.apps-crc.testing
* Log in [<u>https://persisting-pgadmin.apps-crc.testing</u>](https://persisting-pgadmin.apps-crc.testing/) and add new db connection:

| KEY | VALUE |
|--- |--- | 
|Name:|rpi-store|
|Host:|persisting-pg12|
|User:|backend|
|Pass:|secret_pass|

* Verify thah data are present into **types** table
* Verify data persistence by removing the **persisting-pg12** pod

# TASK13:
* Create a Containerfile with the following specifications:
* Use **mariadb** image: repo.localdomain:5000/mariadb
* Copy ~/dox80-env/do188/sql/beer.sql in /docker-entrypoint-initdb.d folder inside to container 
* Send 2 build Arguments: **DB_PASSWD** and **DB_ROOT_PASSWD** 
* Set 4 Variables:

| KEY | VALUE |
|--- |--- | 
|MYSQL_DATABASE=|beer|
|MYSQL_USER=|admin|
|MYSQL_PASSWORD=|
|MYSQL_ROOT_PASSWORD=|

* MYSQL_PASSWORD and **MYSQL_ROOT_PASSWORD** will take values from build parameters **DB_PASSWD** and **DB_ROOT_PASSWD**
* Expose on port **3306**
* Create image named **mariadb-test** and pass build params admin for both parameters 
* Create container **mariadb-test** expose local port **3306** and container port **3306**
* Check with this query: 

```
echo "select \* from beer.types" | mysql -uadmin -padmin -P3306 -h 127.0.0.1
```

# TASK14:

* Create a multi-container based on Wordpress CMS
* Create network **my-app**
* Create volume **my-db-vol**
* Create volume **my-app-vol**
* Create container named **wordpress-db** and set this environments:

| KEY | VALUE |
|--- |--- | 
|MYSQL_ROOT_PASSWORD=|mypass|
|MYSQL_PASSWORD=|mypass|
|MYSQL_DATABASE=|wordpress_db|
|MYSQL_USER=|dbuser|

* attach volume **my-db-vol** to /var/lib/mysql
* attach container to **my-app** network
* using image repo.localdomain:5000/mariadb:latest
* Create container named **wordpress-app** and set this environments:

| KEY | VALUE |
|--- |--- | 
|WORDPRESS_DB_HOST=|wordpress-db:3306|
|WORDPRESS_DB_NAME=|wordpress_db|
|WORDPRESS_DB_USER=|dbuser|
|WORDPRESS_DB_PASSWORD=|mypass|

* expose on port **8080:80**
* attach volume **my-app-vol** to /var/www/html
* attach container to **my-app** network
* using image repo.localdomain:5000/wordpress:latest
* Optionally, try replicating the exercise using podman-compose

# TASK15:

* Create container using custom image multi-stage
* Edit incomplete ~/dox80-env/do188/jdk-maven/Containerfile and complete it
* Use the comments in the file to guide you through the process of creating a Podman Stack with the following requirements
* Build local image with name and tag **maven:mstage** respectively
* Run container named mstage with flag **-it --rm **and make sure you see the **Hello World!** output
