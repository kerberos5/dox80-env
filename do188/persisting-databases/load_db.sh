#!/bin/bash

START_DIR="/opt/app-root/src/postgresql-start"

run_sql_script () {
   SQL_FILE=$1

   psql -U postgres \
      -d $POSTGRESQL_DATABASE \
      -f $SQL_FILE
}

run_sql_script $START_DIR/rpi-store-ddl.sql
