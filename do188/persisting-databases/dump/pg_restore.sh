#!/bin/bash
PG_USER=backend
PG_DB=rpi-store

podman cp /tmp/db_dump persisting-pg13:/tmp
echo "copio il dump sul nuovo container persisting-pg13"

podman exec -it persisting-pg13 pg_restore -d $PG_DB /tmp/db_dump
echo "ripristino i dati dal dump"

podman exec -it persisting-pg13 psql -U $PG_USER -d $PG_DB -c 'select * from types'
echo "verifico i dati nella tabella types"