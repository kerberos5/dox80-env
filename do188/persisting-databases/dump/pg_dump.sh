#!/bin/bash
PG_USER=backend
PG_DB=rpi-store

podman exec -it persisting-pg12 /bin/bash -c "pg_dump -Fc $PG_DB -f /tmp/db_dump"
echo "dump eseguito!"

podman cp persisting-pg12:/tmp/db_dump /tmp/db_dump
echo "dump copiato in locale al path /tmp/db_dump"