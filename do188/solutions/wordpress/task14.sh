#!/bin/bash
podman network create my-app
echo "network my-app successfully created!"
podman volume create my-db-vol
echo "volume my-db-vol successfully created!"
podman volume create my-app-vol
echo "volume my-app-vol successfully created!"

podman run -d --rm --name wordpress-db \
-e MYSQL_ROOT_PASSWORD=mypass \
-e MYSQL_PASSWORD=mypass \
-e MYSQL_DATABASE=wordpress_db \
-e MYSQL_USER=dbuser \
--network my-app \
-v my-db-vol:/var/lib/mysql \
-p 3306:3306 \
repo.localdomain:5000/mariadb:latest
echo "Wordpress database successfully created!"
sleep 3

podman run -d --rm --name wordpress-app \
-e WORDPRESS_DB_HOST=wordpress-db:3306 \
-e WORDPRESS_DB_NAME=wordpress_db \
-e WORDPRESS_DB_USER=dbuser \
-e WORDPRESS_DB_PASSWORD=mypass \
-p 8080:80 \
-v my-app-vol:/var/www/html \
--network my-app \
repo.localdomain:5000/wordpress:latest
echo "Wordpress FE successfully created!"
echo "open http://servera:8080"
