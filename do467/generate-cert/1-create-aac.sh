#!/bin/bash
# Genera una chiave privata per il certificato wildcard:
#openssl genpkey -algorithm RSA -out custom.key
openssl genrsa -out aac.sidi.mpi.it.key 4096

# Genera una richiesta di firma del certificato (CSR) per il certificato wildcard
# utilizzando il file openssl.cnf che hai fornito:
openssl req -new -key aac.sidi.mpi.it.key -out aac.sidi.mpi.it.csr -config openssl-aac.cnf

# Firma il certificato wildcard utilizzando la tua CA custom
openssl x509 -req -in aac.sidi.mpi.it.csr -out aac.sidi.mpi.it.crt -CA ca.crt -CAkey ca.key -CAcreateserial -extfile openssl-aac.cnf -extensions v3_req -days 365
