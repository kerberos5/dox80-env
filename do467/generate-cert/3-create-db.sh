#!/bin/bash
# Genera una chiave privata per il certificato wildcard:
#openssl genpkey -algorithm RSA -out custom.key
openssl genrsa -out db.sidi.mpi.it.key 4096

# Genera una richiesta di firma del certificato (CSR) per il certificato wildcard
# utilizzando il file openssl.cnf che hai fornito:
openssl req -new -key db.sidi.mpi.it.key -out db.sidi.mpi.it.csr -config openssl-db.cnf

# Firma il certificato wildcard utilizzando la tua CA custom
openssl x509 -req -in db.sidi.mpi.it.csr -out db.sidi.mpi.it.crt -CA ca.crt -CAkey ca.key -CAcreateserial -extfile openssl-db.cnf -extensions v3_req -days 365
