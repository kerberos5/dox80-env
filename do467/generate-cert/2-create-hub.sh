#!/bin/bash
# Genera una chiave privata per il certificato wildcard:
#openssl genpkey -algorithm RSA -out custom.key
openssl genrsa -out hub.sidi.mpi.it.key 4096

# Genera una richiesta di firma del certificato (CSR) per il certificato wildcard
# utilizzando il file openssl.cnf che hai fornito:
openssl req -new -key hub.sidi.mpi.it.key -out hub.sidi.mpi.it.csr -config openssl-hub.cnf

# Firma il certificato wildcard utilizzando la tua CA custom
openssl x509 -req -in hub.sidi.mpi.it.csr -out hub.sidi.mpi.it.crt -CA ca.crt -CAkey ca.key -CAcreateserial -extfile openssl-hub.cnf -extensions v3_req -days 365
