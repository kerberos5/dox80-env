# Q1. Installing Automation Controller
Install an automation controller, a private automation hub, and an external database in the server
The packages is locate in ~/AAP on tmvlocp Fedora Workstation

| KEY | VALUE |
|--- |--- |
|controller | aac.sidi.mpi.it |
|automationhub | hub.sidi.mpi.it |
|database | db.sidi.mpi.it |
|admin_password  | redhat |
|pg_host		 | db.sidi.mpi.it |
|pg_password     | redhat |
|registry_url	 | hub.sidi.mpi.it |
|registry_username | admin |
|registry_password | redhat |
|certificates      | ~/AAP/certs |

Once Installed Automation Controller should be available at the following URL:  
* https://aac.sidi.mpi.it/#/login
* https://hub.sidi.mpi.it/ui/

# Q2. Upload collection to Automation Hub
On server student@repo perform following tasks  

* Upload collections available at  
~/dox80-env/do467/contentcollections/community/**containers-podman-1.15.3.tar.gz** to **containers**.podman content collection

* Upload collections available at  
~/dox80-env/do467/contentcollections/certified/**ansible-posix-1.5.4.tar.gz** to **ansible**.posix content collection.


# Q3. Managing Automation Controller
Create the following users and manage team.  

Create a user called **john**  
| KEY | VALUE |
|--- |--- |
|First Name:| John|
|Last Name:| Louis|
|Email:| john@lab.example.com|
|Username:| john|
|Password:| redhat@123|
|User Type:| Normal User|
|Organization:| Default|
	
Create a user called **Rachel**
| KEY | VALUE |
|--- |--- |
|First Name:| Rachel
|Last Name:| Rattus
|Email:| rachel@lab.example.com
|Username:| rachel
|Password:| redhat@123
|User Type:| Normal User
|Organization:| Default
	
Create the team using the following information:
| KEY | VALUE |
|--- |--- |
|Name:| Application|
|Description:| App Team|
|Organization:| Default|
|Members:| john|

| KEY | VALUE |
|--- |--- |
|Name:| Testing|
|Description:| Test Team|
|Organization:| Default|
|Members:| rachel|
	
# Q4. Managing Automation Hub
Navigate to https://hub.sidi.mpi.it/ui/ and complete the following  

* Create a new group called **appdev** and assign permissions to **manage** ansible **content collections** and **containers** in private automation hub
* Create a new user called **donny** with email **donny@lab.example.com** and password as **redhat123** and add the user to the **appdev** group
* Create a new group called **image admins** and assign permissions to **manage** **images** in automation hub
* Create a new group called **deployers** and assign **all** the permissions to all the objects in automation hub
* Create a new user called **martin** with email **martin@lab.example.com** and password as **redhat123** and add the user to the **deployers** group in automation hub.
* Create a new user called **simonds** as a **super user** and password as **redhat123** in the automation hub

# Q5. Overview
Establish following structure of jobs and relevant objects available in automation controller  

* Create static inventory named **acme_dev**
* Add **acme_dev_servers** group in **acme_dev** inventory
* Add **servera.localdomain** in **acme_dev_servers** group
* Assign the **Admin** role on the **acme_dev** inventory to the **Application** team.
* Create a static inventory named **acme_test**
* Create the **acme_test_servers** group in the **acme_test** inventory
* Add the **serverb.localdomain** host to the **acme_test_servers** group in the **acme_test** inventory
* Assign the **Admin** role on the **acme_test** inventory to the **Testing** team
* Assign the **Use** role on the **acme_test** inventory to the **Application** team.

# Q6. Manage Credentials
Create the following credentials needed for the following tasks

| KEY | VALUE |
|--- |--- |
|Type:|SCM|
|Name:|GIT_Student|
|Organization:|Default|
|Username:|student|
|Password:|Giorgi@23|

| KEY | VALUE |
|--- |--- |
|Type:|Machine|
|Name:|MACHINE_Student|
|Username:|student|
|Password:|redhat|
|Organization:|Default|
|PE Method:|sudo|
|PE Username:|root|

# Q7. Managing Project
Create new projects and manage them in Ansible Controller.

* Create a new project called **My Webservers DEV** in default organization
* Project URL: http://gitlab-repo.localdomain:8080/student/my_webservers_dev.git
* Assign the Application team the Admin role on the **My Webservers DEV** project

* Create a new project called **My Webservers TEST** in default organization
* Project URL: http://gitlab-repo.localdomain:8080/student/my_webservers_test.git
* Assign the Testing team the Admin role on the **My Webservers TEST** project

# Q8. Managing Template
Create a new job template called acme_dev_webservers_setup with the following information:  

| KEY | VALUE |
|--- |--- |
|Job Type:|Run|
|Inventory:|acme_dev|
|Project:|My Webservers DEV|
|Playbook:|apache-setup.yml|
|myenv:|developer|  

Give the Application team the **Admin** role on the **acme_dev_webservers_setup** job template

Create a new job template called **acme_test_webservers_setup** with the following information:  

| KEY | VALUE |
|--- |--- |
|Job Type:|Run|
|Inventory:|acme_test|
|Project:|My Webservers TEST|
|Playbook:|apache-setup.yml|
|myenv:|production|  

Give the **Testing** team the **Admin** role on the **acme_test_webservers_setup** job template  
Give the **Application** team the **Execute** role on the **acme_test_webservers_setup** job template  

# Q9. Notification
Create the new notification template using the following information  

| KEY | VALUE |
|--- |--- |
|Name:|Job Status|
|Organization:|Default|
|Type:|E-mail|
|Host:|serverb.localdomain|
|Recipient:|student@localdomain|
|Sender e-mail:|awx@localdomain|
|Port:|25|
|Timeout:|30|

# Q10. Creating Workflow
Create a **From Dev to Test** workflow template with below specification and the  
workflow template should trigger a notification in case of **success** or **failure**  

* The workflow should use the project **My Webservers DEV**, **My Webservers TEST** consecutively after **success**
* The workflow should use the template **acme_dev_webservers_setup**, **acme_test_webservers_setup** consecutively after **success**
* It should use the notification **Job status** to indicate the **success** or the **failure**

# Q11. Controller Backup
Backup the existing Red Hat Ansible Controller installation, all your will be saved in the backup  

# Q12. API Jobs
Create a script file in the controller machine at /root/**api-job-execution.sh**  
When the script file is launched it should execute the **From Dev to Test** workflow template silently

