# This is a solution for the nginx task
FROM docker.io/kerberos5/mycentos:8.0
MAINTAINER "Daniele Trabucco" "trb.dnl@gmail.com"

RUN yum install nginx -y

EXPOSE 80

COPY index.html /usr/share/nginx/html
COPY duffman.png /usr/share/nginx/html

CMD [ "nginx", "-g", "daemon off;" ]
