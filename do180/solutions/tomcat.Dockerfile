# dockerfile to build image for httpd.2.4
# start from Centos8 custom
FROM docker.io/kerberos5/mycentos:8.0

ENV PORT 8080

# file author / maintainer
MAINTAINER "Daniele Trabucco" "trb.dnl@gmail.com"

RUN yum -y install sudo openssh-clients unzip java-1.8.0-openjdk-devel && \
    yum clean all

# add a user for the application, with sudo permissions
RUN useradd -m tomcat ; echo tomcat: | chpasswd ; usermod -a -G root tomcat

# create workdir
RUN mkdir -p /opt/tomcat

WORKDIR /opt/tomcat
RUN curl -O http://servera.localdomain/repo/apache-tomcat-8.5.85.tar.gz
RUN tar xvfz apache*.tar.gz
RUN chown -R tomcat:root /opt/tomcat/apache-tomcat-8.5.85

ENV TOMCAT_HOME /opt/tomcat/apache-tomcat-8.5.85

RUN java -version

COPY ./snoop.war $TOMCAT_HOME/webapps/

EXPOSE $PORT

ENTRYPOINT $TOMCAT_HOME/bin/catalina.sh run

USER tomcat

CMD /bin/bash
