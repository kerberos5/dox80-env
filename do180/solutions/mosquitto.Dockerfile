# This is a solution for the mosquitto task
FROM docker.io/kerberos5/mycentos:8.0
MAINTAINER "Daniele Trabucco" "trb.dnl@gmail.com"

RUN adduser -ms /bin/bash duffman

RUN yum install -y epel-release && \
    yum install -y mosquitto

ADD colors.tar /
COPY skeeter.sh /skeeter.sh

RUN chmod +x /skeeter.sh

EXPOSE 1883

USER duffman

ENTRYPOINT /skeeter.sh
