#!/bin/bash
# Set environment variables:
DB_NAME='wordpressdb'
DB_PASS='password'
DB_USER='wordpressusr'
POD_NAME='wordpress_with_mariadb'
CONTAINER_NAME_DB='wordpress_db'
CONTAINER_NAME_WP='wordpress'

podman run -d --pod $POD_NAME \
-e WORDPRESS_DB_HOST=127.0.0.1:3306 \
-e WORDPRESS_DB_NAME=$DB_NAME \
-e WORDPRESS_DB_USER=$DB_USER \
-e WORDPRESS_DB_PASSWORD=$DB_PASS \
--name $CONTAINER_NAME_WP -v "/home/kerberos5/wpweb":/var/www/html:Z \
servera.localdomain:5000/wordpress:latest
