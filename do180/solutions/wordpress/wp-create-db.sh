#!/bin/bash
# Set environment variables:
DB_NAME='wordpressdb'
DB_PASS='password'
DB_USER='wordpressusr'
POD_NAME='wordpress_with_mariadb'
CONTAINER_NAME_DB='wordpress_db'
CONTAINER_NAME_WP='wordpress'

podman run -d --pod $POD_NAME \
-e MYSQL_ROOT_PASSWORD=$DB_PASS \
-e MYSQL_PASSWORD=$DB_PASS \
-e MYSQL_DATABASE=$DB_NAME \
-e MYSQL_USER=$DB_USER \
--name $CONTAINER_NAME_DB -v "/home/kerberos5/wpdb":/var/lib/mysql:Z \
servera.localdomain:5000/mariadb:latest
