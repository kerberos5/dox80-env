# start from Centos8 custom
FROM docker.io/kerberos5/mycentos:8.0

# Set PORT variable
ENV PORT 8080

# file author / maintainer
MAINTAINER "Daniele Trabucco" "trb.dnl@gmail.com"

# install httpd
RUN yum install -y httpd && \
    yum clean all

RUN sed -ri -e "/^Listen 80/c\Listen 0.0.0.0:${PORT}" /etc/httpd/conf/httpd.conf && \
    sed -ri -e "/^#ServerName/c\ServerName 0.0.0.0:${PORT}" /etc/httpd/conf/httpd.conf && \
    chown -R 1001:0 /etc/httpd/logs/ && \
    chown -R 1001:0 /run/httpd/ && \
    chmod -R g=u /etc/httpd/logs/ && \
    chmod -R g=u /run/httpd/

USER 1001

# Expose the custom port that you provided in the ENV var
EXPOSE ${PORT}

# Copy all files under src/ folder to Apache DocumentRoot (/var/www/html)
COPY ./src/ /var/www/html/

# Copy custom config for reverse proxy role
COPY ./conf/ /etc/httpd/conf.d/

# Start Apache in the foreground
CMD ["httpd", "-D", "FOREGROUND"]
