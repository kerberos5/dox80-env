# Environments
| ROLE | DOMAIN | IPADDRESS |
|--- |--- |--- |
| **OCP Cluster API** | https://api.crc.testing:6443 | 172.16.180.177 |
| **OCP Cluster Console** | https://console-openshift-console.apps-crc.testing | 172.16.180.177 |
| **Gitlab Repository** | https://gitlab.com/kerberos5/dox80-env.git | N.D.|
  **HELM Repository** | http://tmvlocp.sidi.mpi.it:8000/charts/ | 172.16.180.177 |

# Cluster credentials

``` bash
# kubeadmin user credentials
~/.crc/machines/crc/kubeadmin-password
```

# Gitlab Credentials
user: kerberos5  
pass:  <hide>

# Q1. Configure the Identity Provider for the Openshift
Create an Htpassd Identity Provider with the name: **htpass-ex280**  
Create the secret for Identity provider users: **htpass-idp-ex280**  
Create the user account **jobs** with password **deluges**  
Create the user account **wozniak** with password **grannies**  
Create the user account **collins** with password **culverins**  
Create the user account **adlerin** with password **artiste**  
Create the user account **armstrong** with password **spacesuits**  

# Q2. Configure Cluster permissions
User **jobs** is able to modify the cluster  
User **wozniak** is able to create project  
User **armstrong** cannot create projects  
User **wozniak** cannot modify the cluster  
Remove the kubeadmin user from the cluster  

# Q3. Configure Project permissions
Create following projects
* apollo
* titan
* gemini
* bluebook
* apache  

User **armstrong** is **admin** for the **apollo** and **titan** project  
User **collins** is able to **view** the **apollo** project  

# Q4. Create Groups and configure permissions
The user account **armstrong** is a member of **commander** group  
The user account **collins** is a member of **pilot** group  
The user account **alderin** is a member of **pilot** group  
Member of the **commander** group have **edit** permissions in the **apollo** project  
Member of the **pilot** group have **view** permissions in the **apollo** project  

# Q5. Configure Quotas for the Project
Create ResourceQuota in **apache** project named **ex280-quota**  
The amount of memory consumed across all containers may not exceed **1Gi**  
The amount of **CPU** across all containers may not exceed **2** full cores  
The maximum number of **replication** **controllers** does not exceed **3**  
The maximum number of **pods** does not exceed **3**  
The maximum number of **services** does not exceed **6**  

# Q6. Configure Limits for the Project
Create a Limit Range in the **bluebook** project name **ex280-limits**  
The amount of **memory** consumed by a single **pod** is between **100Mi** and **300Mi**  
The amount of **cpu** consumed by a single **pod** is between **10m** and **500m**  
The amount of **cpu** consumed by a single **container** is between **10m** and **500m** with a **default** **request** value of **100m**  
The amount of **memory** consumed by a single **container** is between **100Mi** and **300Mi** with a **default** **request** value of **100Mi**  

# Q7. Deploy an application
Deploy the chart name **nginx** in the project **ascii-hall**  
from the repository: http://tmvlocp.sidi.mpi.it:8000/charts/  
the reposistory should be named **do280-repo**  
You may sure telnet or nc command to validate the deployment

# Q8. Scale the Application manually
Scale an application called **hydra** in the project called **lerna**  
The hydra application should be scaled to **five** times  
if the app does not exist then can use **quay.io/redhattraining/hello-world-nginx:v1.0** image

# Q9. Configure Autoscaling for an Application
Configure an autoscaling for the **scale** application in the project **gru** with following specification:
* Minimum number of replicas: 2
* Maximum number of replicas: 5
* Threshold CPU-Percentage: 60
* Application resource of CPU Request: 25m
* Application limits of CPU Limits: 100m
* if the app does not exist then can use **quay.io/redhattraining/hello-world-nginx:v1.0** image

# Q10. Configure and deploy a secure route
Deploy an application called **oxcart** securely in the project called **area51**
The application has self-signed certificate available **~/dox80-env/do280v4.12/certs**  
The application should be reachable at the following url: https://oxcart.apps.ocp4.redhat.ren  
You can use the **docker.io/kerberos5/httpd:2.4** image

# Q11. Configure a Secret
Configure a secret in the **math** project and the name of secret should be **magic**  
The secret should have following key value pairs: 
* Decoder_Ring: ASDA142hfh-gfrhhueo-erfdk345v

# Q12. Use the Secret value for Application Deployment
Configure the environmental variable for the application called **qed** in the **math** project so that it uses the secret **magic**  
After configuring the environmental value for the application, verify that variable are visible into pod  
You can use the **docker.io/kerberos5/httpd:2.4** image

# Q13. Deploy an application called mercury in project bluebook
Create project **bluebook** if not exist  
Deploy app in **~/dox80-env/do280v4.12/nginx-app-bluebook.yaml**  
Fix the problems, the application should produce output

# Q14. Inject configuration data
Using the **quay.io/redhattraining/hello-openshift:latest** image, deploy and application that meets the following requirements  
The application is part of a project named: **czech**  
The application is named: **ernie**  
The application looks for a key named: **RESPONSE**  
Configuration map named: **ex280-cm**  
Once deployed the application is running and available at https://ernie.apps-crc.testing  
Re-deploying the application after making changes to the configmap should result in a corresponding change to the displayed text:
* **six czech cricket critics**

# Q15. Configure a Service Account
Create a service account called **ex280-sa** in the project called **apples**  
This service account should able to run application with any user id

# Q16. Deploy an application
Deploy an application called **oranges** in the project called **apples**
You can use following templates:
* ~/dox80-env/do280v4.12/oranges-deploy.yaml
* ~/dox80-env/do280v4.12/oranges-service.yaml  
This application should use the service account **ex280-sa**  
The Application should produce a valid output at https://oranges.apps-crc.testing

# Q17. Configure a Network Policy
Configure a network policy using **database** and **checker** project with following requirements  
The **database** project has network policy with the name **db-allow-mysql-comm**   
based on pod selector label **app=pg12**  
Communications to the **database** project are restricted to deployments from the **checker** projects  
The network policy is filtered by project selector using the **team=devsecops** label and pod **checker** using the **app=pgadmin** label  
The application can establish a connections to port **5432/TCP**  
You can check your work login into pgadmin console and connect a postgres db at url: https://pgadmin.apps-crc.testing  
```
# Connection to database rpi-store from pgadmin console
Login:
- user: student@redhat.com
- pass: secret
General:
- Name: rpi-store
Connection:
- Hostname: persisting-pg12.database.svc.cluster.local
- Username: backend
- Password: secret_pass
```
You can use following command for deploing the applications:

```
# Deploy database
oc new-app --name persisting-pg12 \
-l app=pg12 \
-e POSTGRESQL_USER=backend \
-e POSTGRESQL_PASSWORD=secret_pass \
-e POSTGRESQL_DATABASE=rpi-store \
registry.redhat.io/rhel8/postgresql-12:latest \
-n database
```
```
# Deploy pgadmin
oc new-app --name pgadmin \
-l app=pgadmin \
-e PGADMIN_DEFAULT_EMAIL=student@redhat.com \
-e PGADMIN_DEFAULT_PASSWORD=secret \
docker.io/dpage/pgadmin4:latest \
-n checker
```
```
# injects the data into rpi-store database
oc cp ~/dox80-env/do280v4.12/rpi-store-ddl.sql -n database <pod>:/tmp
oc rsh -n database <pod> bash -c 'psql -U postgres -d rpi-store -f /tmp/rpi-store-ddl.sql'
```

# Q18. Persistent storage
Configure a persistent volume with following requirements

| KEY | VALUE |
|--- |--- |
|Name:|landing-pv|
|Access mode:|ReadWriteOnce|
|Size:|1Gi|
|The reclaim policy:|matches the **nfs-storage** storage class|
|path:|/shares/html|
|server:|tmvlocp.sidi.mpi.it|

Configure a persistent volume claim with following requirements:

| KEY | VALUE |
|--- |--- |
|Name:|landing-pvc|
|The access mode:|same as the persistent volume|
|Size:|same as the persistent volume|

Deploy the application with following requirements:  

The application exists in a project called **page**  
The application uses a deployment called **landing**  
The application uses the images located at **docker.io/kerberos5/httpd:2.4**  
The httpd mountpoint in **/var/www/html**  
The application uses **3** pods  
The application is accessable at https://landing.apps-crc.testing  

# Q19. Install Operator
Install the **file-intergrity** operator with following requirements:  
The operator installed in the **openshift-file-integrity** project  
The approval strategy is **Automatic**  
Cluster monitoring is credit for the **openshift-file-intergrity** project  
Apply the FileIntegrity template in **~/dox80-env/do280v4.12/worker-fileintegrity.yaml**  
Check with following command:  
```
oc get -n openshift-file-integrity fileintegrities/worker-fileintegrity  -o jsonpath="{ .status.phase }" && echo
```

# Q20. Cronjob
Create a cron job using the image at **registry.redhat.io/openshift4/ose-cli:v4.10**  
The cron job name is **job-rusner**  
The cron job run at **04:05** on the **2nd** day of every month  
The successed job history limit in **14**  
The service account and service account name is **magna**  
The cron job runs in the project called **elemtum**  

# Q21. Configure a project template
Configure your OpenShift cluster so that new project are created with limit using following requirements:  
The name of limit range is **PROJECT_NAME-limits** where **PROJECT_NAME** is the name of the project created using oc new-project  
The amount of memory consumed by a single container is between **128Mi** and **1Gi** with default of **512Mi** and a default request of **256Mi**  

# Q22. Collect cluster Information for Red Hat support
Collect the default support information for your OpenShift cluster with following requirements:  
The data is stored as compressed tar archive using tar **cvaf**  
The name of the comporessed tar archive is: **ex280-ocp.clusterID.tar.gz**  
where **clusterID** is the unique identifier of your Openshift cluster  

# Q23. Configure a health probe
An application named **atlas** has been deployed with single container is the **mercary** project  
Implement a **liveness** **probe** for this container that exec following requirements:  
* The probe monitors **liveness** by performing a TCP socket check on port **8080**
* The probe has an initial delay of **10** second and a timeout of **30** second
* You can use the **docker.io/kerberos5/httpd:2.4** image
* Your changes can survive a rebuild