# Environments
| ROLE | DOMAIN | IPADDRESS |
|--- |--- |--- |
| **OCP Cluster API** | https://api.crc.testing:6443 | 172.16.180.177 |
| **OCP Cluster Console** | https://console-openshift-console.apps-crc.testing | 172.16.180.177 |
| **Gitlab Repository** | https://gitlab.com/kerberos5/dox80-env.git | N.D.|
  **HELM Repository** | http://tmvlocp.sidi.mpi.it:8000/charts/ | 172.16.180.177 |

# Cluster credentials
user: admin  
pass: review  

# Gitlab Credentials
user: kerberos5  
pass:  <hide>

# Q1. Configure the Identity Provider for the Openshift

``` bash
# Installa httpd-tools
$ sudo dnf install httpd-tools -y
```

``` bash
# Crea file con le credenziali
$ htpasswd -c -b -B htpass-idp-ex280 jobs deluges
$ htpasswd -b -B htpass-idp-ex280 wozniak grannies
$ htpasswd -b -B htpass-idp-ex280 collins culverins
$ htpasswd -b -B htpass-idp-ex280 adlerin artiste
$ htpasswd -b -B htpass-idp-ex280 armstrong spacesuits

# Crea secret
$ oc create secret generic htpass-idp-ex280 --from-file htpasswd=htpass-idp-ex280 -n openshift-config

# Salva una copia di oauth
$ oc get oauth/cluster -o yaml > /tmp/oauth.yaml

# Crea da console il nuovo provider con nome htpass-ex280

``` yaml
# oppure edit /tmp/oauth.yaml come nell'esempio
apiVersion: config.openshift.io/v1
kind: OAuth
metadata:
  name: cluster
spec:
  identityProviders:
  - htpasswd:
      fileData:
        name: htpass-idp-ex280
    mappingMethod: claim
    name: htpass-ex280
    type: HTPasswd
  - htpasswd:
      fileData:
        name: htpass-secret
    mappingMethod: claim
    name: developer
    type: HTPasswd
  - ldap:
      attributes:
        email:
          - mail
        id:
          - dn
        name:
          - cn
        preferredUsername:
          - uid
      bindDN: cn=admin,dc=redhat,dc=ren
      bindPassword:
        name: ldap-bind-password
      ca:
        name: ldap-ca
      insecure: false
      url: ldaps://ldap-dev.sidi.mpi.it:10636/ou=users,dc=redhat,dc=ren?uid
    mappingMethod: claim
    name: ldap
    type: LDAP
  templates:
    login:
      name: login-template
  tokenConfig:
    accessTokenMaxAgeSeconds: 0
```

``` bash
# Applica il nuovo manifest con
$ oc replace -f /tmp/oauth.yaml
```

``` bash
# Verifica i pod di autenticazione che si aggiornano
$ watch oc get pods -n openshift-authentication

# Esegui gli accessi su tutti gli utenti
$ oc login -u <user> -p <password>
```

# Q2. Configure Cluster permissions

``` bash
# Esegui la login come kubeadmin e dai permessi agli utenti
$ oc adm policy add-cluster-role-to-user cluster-admin jobs
$ oc get clusterrolebindings -o wide | grep self-

# Dai la possibilità all'utente wozniak di creare progetti
$ oc adm policy add-cluster-role-to-user --rolebinding-name self-provisioners self-provisioner wozniak

# Rimuovi la possibilità di creare progetti al gruppo oauth
$ oc adm policy remove-cluster-role-from-group self-provisioner system:authenticated:oauth

# Per non far ripristinare la situazione a seguito del riavvio dei pod di apiserver esegui
$ oc annotate clusterrolebinding.rbac self-provisioners 'rbac.authorization.kubernetes.io/autoupdate=false' --overwrite

# Rimuovi kubeadmin (ATTENZIONE!!! accertati che almeno un utente abbia privilegi cluster-admin)
$ oc delete secrets kubeadmin -n kube-system
```

# Q3. Configure Project permissions

``` bash
$ for prj in {apollo,titan,gemini,bluebook,apache}; do oc new-project $prj; done
$ oc get projects | grep -E -v 'openshift|kube'
$ oc adm policy add-role-to-user admin armstrong -n apollo
$ oc adm policy add-role-to-user admin armstrong -n titan
$ oc adm policy add-role-to-user view collins -n apollo
```

# Q4. Create Groups and configure permissions

``` bash
$ oc adm groups new commander
$ oc adm groups new pilot
$ oc adm groups add-users commander armstrong
$ oc adm groups add-users pilot collins
$ oc adm groups add-users pilot alderin
$ oc adm policy add-role-to-group edit commander -n apollo
$ oc adm policy add-role-to-group view pilot -n apollo
```

# Q5. Configure Quotas for the Project

``` bash
$ oc project apache
$ oc create quota ex280-quota --hard=cpu=2,memory=1G,pods=3,services=6,replicationcontrollers=3
$ oc describe quota ex280-quota
```

# Q6. Configure Limits for the Project
Aiutati con la console entra nel progetto **bluebook** e crea i limits come segue

``` yaml
apiVersion: v1
kind: LimitRange
metadata:
  name: ex280-limits
  namespace: bluebook
spec:
  limits:
    - type: Container
      defaultRequest:
        memory: 100Mi
        cpu: 100m
      min:
        cpu: 10m
        memory: 100Mi
      max:
        cpu: 500m
        memory: 300Mi
    - type: Pod
      min:
        cpu: 10m
        memory: 100Mi
      max:
        cpu: 500m
        memory: 300Mi
```

# Q7. Deploy an application
Deploy the chart name **redhat-cenima** in the project **ascii-hall**  
from the repository: http://tmvlocp.sidi.mpi.it:8000/charts/  
the reposistory should be named **do280-repo**  
You may sure telnet or nc command to validate the deployment

``` bash
# Installa helm
$ sudo dnf install helm -y
$ helm completion bash > /tmp/helm-kubernetes.sh
$ sudo cp /tmp/helm-kubernetes.sh /etc/bash_completion.d/helm-kubernetes.sh
```

``` bash
$ oc new-project ascii-hall
$ helm repo ls
$ helm repo add do280-repo http://tmvlocp.sidi.mpi.it:8000/charts/
$ helm search repo do280-repo --versions
$ helm install redhat-cenima do280-repo/nginx -n ascii-hall 
```

# Q8. Scale the Application manually

``` bash
$ oc new-project lerna
$ oc scale deployment/hydra --replicas=5
```

# Q9. Configure Autoscaling for an Application

``` bash
$ oc new-project gru
$ oc autoscale deployment/gru-nginx --min 2 --max 5 --cpu-percent 60
$ oc set resources deployment gru-nginx --limits=cpu=100m --requests=cpu=25m
```

# Q10. Configure and deploy a secure route

``` bash
$ oc project lerna
$ oc get deploy
$ oc get svc
$ oc create route edge oxcart \
--service oxcart \
--hostname oxcart.apps.ocp4.redhat.ren \
--port 8080 --cert ~/dox80-env/do280v4.12/certs/combined.crt \
--key ~/dox80-env/do280v4.12/certs/tls.key
```

# Q11. Configure a Secret

``` bash
$ oc project math
$ oc create secret generic magic --from-literal Decoder_Ring=ASDA142hfh-gfrhhueo-erfdk345v
```

# Q12. Use the Secret value for Application Deployment

``` bash
$ oc project math
$ oc get deploy
$ oc set env deployment/qed --from secret/magic
$ oc exec -it pod/qed-58666c966-zcvwh bash -- env | grep -i Decoder_Ring
```

# Q13. Deploy an application called mercury in project bluebook

``` bash
$ oc new-project bluebook
$ oc apply -f ~/dox80-env/do280v4.12/nginx-app-bluebook.yaml
$ oc get pod
$ oc get endpoints
$ oc get events
$ oc edit deployment/nginx-app
```

# Q14. Inject configuration data

``` bash
$ oc new-project czech
$ oc new-app --name ernie --image quay.io/redhattraining/hello-openshift:latest
$ oc create route edge ernie --service ernie --hostname ernie.apps-crc.testing
$ oc create configmap ex280-cm --from-literal RESPONSE='six czech cricket critics'
$ oc set env deployment/ernie --from configmap/ex280-cm
$ curl -vk https://ernie.apps-crc.testing
```

# Q15. Configure a Service Account

``` bash
$ oc new-project apples
$ oc create sa ex280-sa
$ oc adm policy add-scc-to-user anyuid -z ex280-sa
```

# Q16. Deploy an application

``` bash
$ oc new-project apples
$ oc apply -f ~/dox80-env/do280v4.12/oranges-deploy.yaml
$ oc apply -f ~/dox80-env/do280v4.12/oranges-service.yaml
$ oc set sa deployment/oranges ex280-sa
$ oc edit svc oranges
$ oc expose svc oranges --hostname oranges.apps-crc.testing
```

# Q17. Configure a Network Policy
You can use following command for deploing the applications:

``` bash
# Create project
$ oc new-project database
# Deploy database
$ oc new-app --name persisting-pg12 \
-l app=pg12 \
-e POSTGRESQL_USER=backend \
-e POSTGRESQL_PASSWORD=secret_pass \
-e POSTGRESQL_DATABASE=rpi-store \
registry.redhat.io/rhel8/postgresql-12:latest \
-n database
```

``` bash
# Create project
$ oc new-project checker
# Deploy pgadmin
oc new-app --name pgadmin \
-l app=pgadmin \
-e PGADMIN_DEFAULT_EMAIL=student@redhat.com \
-e PGADMIN_DEFAULT_PASSWORD=secret \
docker.io/dpage/pgadmin4:latest \
-n checker

$ oc create route edge pgadmin --service pgadmin --hostname pgadmin.apps-crc.testing
```

``` bash
# injects the data into rpi-store database
oc cp ~/dox80-env/do280v4.12/rpi-store-ddl.sql -n database <pod>:/tmp
oc rsh -n database <pod> bash -c 'psql -U postgres -d rpi-store -f /tmp/rpi-store-ddl.sql'
```
```
# Connection to database rpi-store from pgadmin console
Login:
- user: student@redhat.com
- pass: secret
General:
- Name: rpi-store
Connection:
- Hostname: persisting-pg12.database.svc.cluster.local
- Username: backend
- Password: secret_pass
```
``` bash
$ oc get pods -n database --show-labels
$ oc get pods -n checker --show-labels
$ oc label namespaces checker team=devsecops
```
** Crea la NP da console ocp nel project database come da istruzioni

# Q18. Persistent storage

``` bash
oc new-project page
oc new-app --name landing --image docker.io/kerberos5/httpd:2.4
oc create route edge landing --service landing --hostname landing.apps-crc.testing --port 8080
```

Configure a persistent volume with following requirements

``` yaml
apiVersion: v1
kind: PersistentVolume
metadata:
  name: landing-pv
spec:
  capacity:
    storage: 1Gi
  accessModes:
    - ReadWriteOnce
  persistentVolumeReclaimPolicy: Retain
  storageClassName: nfs-storage
  nfs:
    path: /shares/html
    server: tmvlocp.sidi.mpi.it
```

Configure a persistent volume claim with following requirements:

``` yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: landing-pvc
  namespace: page
spec:
  accessModes:
    - ReadWriteOnce
  volumeMode: Filesystem
  resources:
    requests:
      storage: 1Gi
```

``` bash
$ oc set volumes deployment/landing --add --name landing-pv --type pvc --claim-name landing-pvc --mount-path /var/www/html
$ oc scale deployment/landing --replicas=3 
```

# Q19. Install Operator
Installa **file-intergrity** operator with the approval strategy is **Automatic**  

Check with following command:  
``` bash
$ oc apply -f ~/dox80-env/do280v4.12/worker-fileintegrity.yaml
$ oc get -n openshift-file-integrity fileintegrities/worker-fileintegrity  -o jsonpath="{ .status.phase }" && echo
```

# Q20. Cronjob

``` bash
$ oc new-project elemtum
$ oc create sa magna
``` 
Crea un cronjob da console usando l'editor integrato in questo modo

``` yaml
apiVersion: batch/v1
kind: CronJob
metadata:
  name: job-rusner
  namespace: elemtum
spec:
  schedule: '5 4 2 * *'
  successfulJobsHistoryLimit: 14
  jobTemplate:
    spec:
      template:
        spec:
          containers:
            - name: job-rusner
              image: registry.redhat.io/openshift4/ose-cli:v4.10
              args:
                - /bin/sh
                - '-c'
                - date; echo Hello from the Kubernetes cluster
          restartPolicy: OnFailure
          serviceAccount: magna
          serviceAccountName: magna
```

# Q21. Configure a project template

``` bash
$ oc adm create-bootstrap-project-template -o yaml > project-request.yaml
```
Edita il template project-request.yaml e aggiungi la sezione dei limitis range come segue

``` yaml
---
apiVersion: template.openshift.io/v1
kind: Template
metadata:
  name: project-request
  namespace: openshift-config
objects:
- apiVersion: project.openshift.io/v1
  kind: Project
  metadata:
    annotations:
      openshift.io/description: ${PROJECT_DESCRIPTION}
      openshift.io/display-name: ${PROJECT_DISPLAYNAME}
      openshift.io/requester: ${PROJECT_REQUESTING_USER}
    name: ${PROJECT_NAME}
  spec: {}
  status: {}
- apiVersion: rbac.authorization.k8s.io/v1
  kind: RoleBinding
  metadata:
    name: admin
    namespace: ${PROJECT_NAME}
  roleRef:
    apiGroup: rbac.authorization.k8s.io
    kind: ClusterRole
    name: admin
  subjects:
  - apiGroup: rbac.authorization.k8s.io
    kind: User
    name: ${PROJECT_ADMIN_USER}
- apiVersion: v1
  kind: LimitRange
  metadata:
    name: ${PROJECT_NAME}-limits
    namespace: ${PROJECT_NAME}
  spec:
    limits:
      - default:
          memory: 512Mi
        defaultRequest:
          memory: 256Mi
        min:
          memory: 128Mi
        max:
          memory: 1Gi
        type: Container
parameters:
- name: PROJECT_NAME
- name: PROJECT_DISPLAYNAME
- name: PROJECT_DESCRIPTION
- name: PROJECT_ADMIN_USER
- name: PROJECT_REQUESTING_USER
```

Applica il template
``` bash
$ oc apply -f project-request.yaml
```

Edita anche da console la CR project/cluster in questo modo

``` yaml
apiVersion: config.openshift.io/v1
kind: Project
metadata:
  name: cluster
spec:
  projectRequestTemplate:
    name: project-request
```

Verifica che i pods in openshift-apiserver si ricreino correttamente

``` bash
$ watch oc get pod -n openshift-apiserver
```

# Q22. Collect cluster Information for Red Hat support

``` bash
$ oc get clusterversions
$ oc adm must-gather --dest-dir=ex280-ocp
$ tar cvaf ex280-ocp-<CLUSTER_ID>.tar.gz ex280-ocp
$ /usr/local/bin/upload-cluster-data ex280-ocp-<CLUSTER_ID>.tar.gz
```

# Q23. Configure a health probe

``` bash
$ oc new-project mercary
$ oc new-app --name atlas --image docker.io/kerberos5/httpd:2.4
$ oc set probe deployment/atlas --liveness --open-tcp=8080 --initial-delay-seconds=10 --timeout-seconds=30
$ oc describe deploy atlas | grep Live
```