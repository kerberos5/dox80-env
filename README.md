# DOX80-ENV
## Resource for practice exam RedHat DOx80

### Paragraph
``` bash
$ insert your code
$ insert your code

```
## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

## Supported Exams
* AD248
* DO180
* DO188
* DO280
* DO380
* DO374
* DO467